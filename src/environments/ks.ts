// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// export const environment = {
//   production: false,
//   serverUrl: 'http://localhost:1337/api',
//   appUrl: 'https://www.tryion.shop',
//   appImageUrl: 'https://www.tryion.shop/assets/imgs/ionshop.png',
//   appId: 'myAppId',
//   fbId: 'YOUR_FACEBOOK_APP_ID',
//   stripePublicKey: 'YOUR_STRIPE_PUBLIC_KEY',
//   androidHeaderColor: '#222428',
//   defaultLang: 'en',
//   googleClientId: 'YOUR_GOOGLE_CLIENT_ID',
//   currency: {
//     code: 'IQD',
//     display: 'symbol',
//     digitsInfo: '1.0-0',
//   },
// };


export const environment = {
  production: false,
  serverUrl: 'https://ahaseedapp.kreate-soft.com/api',
  appUrl: 'https://ahaseedapp.kreate-soft.com',
  appImageUrl: 'https://ahaseedapp.kreate-soft.com/assets/imgs/ionshop.png',
  appId: 'u1YXpqWsLg',
  fbId: '',
  stripePublicKey: '',
  androidHeaderColor: '#222428',
  defaultLang: 'ar',
  googleClientId: '',
  currency: {
    code: 'د.ع',
    display: 'symbol',
    digitsInfo: '1.0-0',
  },
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
