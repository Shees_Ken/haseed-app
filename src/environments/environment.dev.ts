export const environment = {
  production: true,
  serverUrl: 'https://ahaseedapp.kreate-soft.com/api',
  appUrl: 'https://ahaseedapp.kreate-soft.com',
  appImageUrl: 'https://ahaseedapp.kreate-soft.com/assets/imgs/ionshop.png',
  appId: 'u1YXpqWsLg',
  fbId: '',
  stripePublicKey: '',
  androidHeaderColor: '#222428',
  defaultLang: 'ar',
  googleClientId: '',
  currency: {
    code: 'د.ع',
    display: 'symbol',
    digitsInfo: '1.0-0',
  },
};
