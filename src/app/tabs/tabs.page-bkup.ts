import { Component, ViewChild } from '@angular/core';
import { Platform, IonTabs, NavController } from '@ionic/angular';
import { Preference } from '../services/preference';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss']
})
export class TabsPage {
  selectedIndex = 'home';

  @ViewChild(IonTabs) tabs: IonTabs;

  resetStackTabs = ['home', 'browse', 'cart', 'account'];

  handleTabClick = (event: MouseEvent) => {
    const { tab } = event.composedPath().find((element: any) =>
      element.tagName === 'ION-TAB-BUTTON') as EventTarget & { tab: string };

    let deep = 1;
    let canGoBack = false;

    const deepFn = () => {
      if (this.tabs.outlet.canGoBack(deep, tab)) {
        canGoBack = true;
        deep++;
        deepFn();
      }
    }

    deepFn();

    if (this.resetStackTabs.includes(tab) && canGoBack) {
      event.stopImmediatePropagation();
      return this.tabs.outlet.pop(deep - 1, tab);
    }
  }

  goToOrdersPage() {
    this.router.navigate(["1/orders"])
  }


  constructor(
    public platform: Platform,
    private pubsub: NgxPubSubService,
    public preference: Preference,
    private router: Router,
    private nav: NavController
  ) {
    pubsub.subscribe('sidemenu:openLink', (link) => {
      this.startmenuopenlink(link);
    })
  }

  startmenuopenlink(link){
    this.nav.navigateRoot('1/home');
    this.pubsub.publishEvent('tabs:openlink', link);
  }
}
