import { Injectable } from '@angular/core';
import * as Parse from 'parse';
import { ItemVariation } from './item-variation';
import { Brand } from './brand';
import { Category } from './category';
import { SubCategory } from './sub-category';
import { Item } from './item';

@Injectable({
    providedIn: 'root'
})

export class ComboItem extends Parse.Object {
    constructor() {
        super('ComboItem');
    }

    static getInstance() {
        return this;
    }

    loadInCloud(params: any = {}): Promise<ComboItem[]> {
        return Parse.Cloud.run('getComboItems', params);
    }

    loadFeaturedComboItems(): Promise<any> {
        const query = new Parse.Query(ComboItem);
        query.equalTo('isFeatured', true);
        query.doesNotExist('deletedAt');
        return query.find();
    }

    loadOne(id: string): Promise<ComboItem> {
        const query = new Parse.Query(ComboItem);
        query.doesNotExist('deletedAt');
        return query.get(id);
    }

    getComboItemName(id: string) {
        const query = new Parse.Query(ComboItem);
        query.equalTo('_id', id);
        query.doesNotExist('deletedAt');
        return query.find();
    }

    load(params: any = {}): Promise<any[]> {

        const queries = []

        if (params.tag) {
          const searchQuery = params.tag.toLowerCase()
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
    
          const queryTag = new Parse.Query('ComboItem')
          queryTag.contains('tags', searchQuery)
          queries.push(queryTag)
    
          const queryCanonical = new Parse.Query('ComboItem')
          queryCanonical.contains('canonical', searchQuery)
          queries.push(queryCanonical)
        }
    
        let mainQuery = new Parse.Query('ComboItem')
    
        if (queries.length) {
          mainQuery = Parse.Query.or(...queries)
        }

        mainQuery.equalTo('status', 'Active');
        mainQuery.ascending('name');
        mainQuery.doesNotExist('deletedAt');
        
        return mainQuery.find();
    }

    like(itemId: string) {
        return Parse.Cloud.run('likeComboItem', { itemId: itemId });
    }

    isLiked(itemId: string): Promise<boolean> {
        return Parse.Cloud.run('isComboItemLiked', { itemId: itemId });
    }

    trackView(itemId: string) {
        return Parse.Cloud.run('trackViewComboItem', { itemId: itemId });
    }

    get objectId(): string {
        return this.id;
    }

    get name(): string {
        return this.get('name');
    }

    get status(): string {
        return this.get('status');
    }

    get slug(): string {
        return this.id + '/' + (this.get('slug') || '');
    }

    get featuredImageThumb(): any {
        return this.get('featuredImageThumb');
    }

    get featuredImage(): any {
        return this.get('featuredImage');
    }

    get isFeatured(): boolean {
        return this.get('isFeatured')
    }

    get isNewArrival(): boolean {
        return this.get('isNewArrival')
    }

    get category(): any {
        return this.get('category')
    }

    get discount(): number {
        return this.get('discount')
    }

    get ratingCount() {
        return this.get('ratingCount');
    }

    get ratingTotal() {
        return this.get('ratingTotal');
    }

    get ratingAvg() {
        return this.get('ratingAvg');
    }

    get deletedAt(): Date {
        return this.get('deletedAt');
    }

    get items(): Item[] {
        const items = this.get('items') || [];
        return items.filter((item: Item) => {
            return typeof item.deletedAt === 'undefined'
        });
    }

}

Parse.Object.registerSubclass('ComboItem', ComboItem);