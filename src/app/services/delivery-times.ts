import { Injectable } from '@angular/core';
import * as Parse from 'parse';

@Injectable({
    providedIn: 'root'
})
export class DeliveryTime extends Parse.Object {

    constructor() {
        super('DeliveryTime');
    }

    static getInstance() {
        return this;
    }

    load(params: any = {}): Promise<DeliveryTime[]> {
        const query = new Parse.Query(DeliveryTime);

        if (params.type) {
            query.equalTo('type', params.type);
        }

        if (params.parent) {
            query.equalTo('parent', params.parent);
        }

        query.ascending(name);
        query.equalTo('isActive', true);
        query.doesNotExist('deletedAt');
        return query.find();
    }

    get objectId(): string {
        return this.get('id');
    }

    get name(): string {
        return this.get('name');
    }

    get zone(): string {
        return this.get('zone');
    }

    get time(): number {
        return this.get('time');
    }

    get sort(): number {
        return this.get('sort');
    }
}

Parse.Object.registerSubclass('DeliveryTime', DeliveryTime);
