import { Injectable } from '@angular/core';
import * as Parse from 'parse';

@Injectable({
    providedIn: 'root'
})
export class Category extends Parse.Object {

    constructor() {
        super('Category');
    }

    static getInstance() {
        return this;
    }

    load(params: any = {}): Promise<any> {

        const queries = []

        if (params.tag) {
          const searchQuery = params.tag.toLowerCase()
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
    
          const queryTag = new Parse.Query('Category')
          queryTag.contains('tags', searchQuery)
          queries.push(queryTag)
    
          const queryCanonical = new Parse.Query('Category')
          queryCanonical.contains('canonical', searchQuery)
          queries.push(queryCanonical)
        }
    
        let mainQuery = new Parse.Query('Category')
    
        if (queries.length) {
          mainQuery = Parse.Query.or(...queries)
        }
    
        if (params.canonical) {
          mainQuery.contains('canonical', params.canonical);
        }
    
        mainQuery.equalTo('status', 'Active');
        mainQuery.ascending('name');
        mainQuery.doesNotExist('deletedAt');
    
        return mainQuery.find();
        // const query = new Parse.Query(Category);

        // if (params.canonical) {
        //     query.contains('canonical', params.canonical);
        // }

        // query.equalTo('status', 'Active');
        // query.ascending('name');
        // query.doesNotExist('deletedAt');

        // return query.find()
    }

    get name(): string {
        return this.get('name');
    }

    get status(): string {
        return this.get('status');
    }

    get slug(): string {
        return this.id + '/' + (this.get('slug') || '');
    }

    get imageThumb(): any {
        return this.get('imageThumb');
    }

    get image(): any {
        return this.get('image');
    }

    get subCategoryCount(): any {
        return this.get('subCategoryCount');
    }

}

Parse.Object.registerSubclass('Category', Category);