import { Injectable } from '@angular/core';
import * as Parse from 'parse';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class Notification extends Parse.Object {
    constructor() {
        super('Notification');
    }

    static getInstance() {
        return this;
    }

    load(): Promise<Notification[]> {
        let user = User.getCurrent();
        const query = new Parse.Query(Notification);
        query.descending('createdAt')
        return query.find();
    }

    get objectId(): string {
        return this.id;
    }

    get heading(): string {
        return this.get('heading');
    }

    get message(): string {
        return this.get('message');
    }

    get users(): any {
        return this.get('users');
    }

    get type(): string {
        return this.get('type');
    }


}

Parse.Object.registerSubclass('Notification', Notification);