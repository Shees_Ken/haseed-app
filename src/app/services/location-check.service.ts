import { Injectable } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
    providedIn: 'root'
})

export class LocationCheckService {

    constructor(
        public alertCtrl: AlertController,
        public plt: Platform,
        public settings: OpenNativeSettings,
        public diagnostic: Diagnostic,
        private androidPermissions: AndroidPermissions,
        private geolocation: Geolocation
    ) { }


    isLocationEnabled() {
        if (this.plt.is("ios")) {
            this.isLocationEnableiOS();
        }

        if (this.plt.is("android")) {
            this.isLocationEnableAndroid();
        }
    }

    isLocationEnableiOS() {

        this.plt.ready().then(() => {
            this.diagnostic.isLocationAuthorized().then(isAuthorized => {
                console.log();
                if (isAuthorized == false) {
                    this.openAppSettingsPermissions();
                }

                if (isAuthorized == true) {

                    this.diagnostic.isLocationAvailable().then((state) => {

                        if (state == false) {
                            this.openLocationSettings()
                        }

                    }).catch(err => {
                        console.log(err);
                    })


                }

            })
        })



    }

    isLocationEnableAndroid() {

        this.plt.ready().then(() => {
            this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
                result => {

                    if (result.hasPermission == false) {

                        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
                            v => {
                                if (v.hasPermission == false) {
                                    this.openAppSettingsPermissions();
                                }
                            }
                        )
                    }

                    if (result.hasPermission == true) {

                        this.diagnostic.isLocationEnabled().then(isAllowed => {
                            if (isAllowed == false) {
                                this.openLocationSettings();
                            }
                        }, err => {
                            console.log(err);
                        })

                    }



                },
                err => {
                    console.log('error here');
                }
            );
        })



    }

    async openAppSettingsPermissions() {
        let confirm = await this.alertCtrl.create({
            backdropDismiss: true,
            header: "Permission",
            message: "يرجى تفعيل الموقع  لتحديد مكانك لتوصيل طلبك",
            buttons: [

                {
                    text: 'تفعيل الموقع',
                    handler: () => {
                        this.settings.open('application_details');
                    }
                }
            ]
        });
        confirm.present();
    }


    async openLocationSettings() {
        let confirm = await this.alertCtrl.create({
            backdropDismiss: true,
            header: 'Location',
            message: "يرجى تفعيل الموقع  لتحديد مكانك لتوصيل طلبك",
            buttons: [
                {
                    text: 'تفعيل الموقع',
                    handler: () => {
                        this.settings.open('location');
                    }
                }
            ]
        });
        confirm.present();
    }

    getLocation() {
        return new Promise((resolve) => {
            this.geolocation.getCurrentPosition().then((resp) => {
                resolve({ lat: resp.coords.latitude, lng: resp.coords.longitude });
            }).catch((error) => {
                console.warn("Error Getting Location", error);
                resolve(null);
            })
        })
    }

}
