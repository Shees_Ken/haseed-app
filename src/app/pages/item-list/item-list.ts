import { Component, Injector, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { Item } from '../../services/item';
import { BasePage } from '../base-page/base-page';
import { SubCategory } from '../../services/sub-category';
import { Category } from '../../services/category';
import { Subject, Observable, merge } from 'rxjs';
import { FilterPage } from '../filter-modal/filter-modal.page';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { ComboItem } from 'src/app/services/combo-item';

@Component({
  selector: 'page-item-list',
  templateUrl: 'item-list.html',
  styleUrls: ['item-list.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('100ms', [animate('300ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class ItemListPage extends BasePage {

  @ViewChild(IonContent, { static: true }) content: IonContent;

  public items: Item[] = [];
  public skeletonArray = Array(32);
  public params: any = {
    page: 0,
    limit: 20
  };

  public suggestions: Item[] = [];

  public loadAndScroll: Observable<any>;
  protected contentLoaded: Subject<any>;

  protected category: Category;
  protected subcategory: SubCategory;
  protected searchText: string;

  public customPopoverOptions: any = {};

  public sortOptions: Array<any>;
  public selectedSortOption: any;

  public showSearch: boolean = false;
  public isComboItemList: boolean = false;

  constructor(injector: Injector,
    private itemService: Item,
    private comboItemService: ComboItem,
    pubsub: NgxPubSubService) {
    super(injector);

    pubsub.subscribe("cart:itemDeleted", this.receivedCartDeleteUpadte.bind(this));
    pubsub.subscribe("cart:itemAdded", this.receivedCartAddUpadte.bind(this));
    pubsub.subscribe("cart:itemIncrement", this.increment.bind(this));
    pubsub.subscribe("cart:itemDecrement", this.decrement.bind(this));
    pubsub.subscribe("cart:checkOut", this.receivedOrderUpadte.bind(this));
    pubsub.subscribe('product:searchOn', (res) => {
      this.showSearch = !this.showSearch;
    })

  }

  ngOnInit() {
    this.isComboItemList = this.getQueryParams().comboList;
    console.log(this.isComboItemList);
    this.buildSortOptions();
    this.setupObservables();
    this.setupQueryParams();
  }

  setupObservables() {

    this.contentLoaded = new Subject();

    this.loadAndScroll = merge(
      this.content.ionScroll,
      this.contentLoaded
    );

  }

  setupQueryParams() {
    this.params.sale = this.getQueryParams().sale;
    this.params.new = this.getQueryParams().new;
    this.params.featured = this.getQueryParams().featured;
    this.params.ratingMin = this.getQueryParams().ratingMin;
    this.params.ratingMax = this.getQueryParams().ratingMax;
    this.params.priceMin = this.getQueryParams().priceMin;
    this.params.priceMax = this.getQueryParams().priceMax;
    this.params.cat = this.getQueryParams().cat;
    this.params.subcat = this.getQueryParams().subcat;
    this.params.brand = this.getQueryParams().brand;
  }

  enableMenuSwipe(): boolean {
    return false;
  }

  async ionViewDidEnter() {

    if (!this.items.length) {
      this.showLoadingView({ showOverlay: false });
      this.loadData();
    }

    let title;
    if (this.isComboItemList) {
      title = await this.getTrans('COMBO_ITEMS');
      this.setPageTitle(title);
    } else {
      title = await this.getTrans('ITEMS');
      this.setPageTitle(title);
    }


    this.setMetaTags({
      title: title
    });
  }

  buildSortOptions() {

    this.sortOptions = [{
      sortBy: 'desc', sortByField: 'createdAt',
    }, {
      sortBy: 'asc', sortByField: 'netPrice',
    }, {
      sortBy: 'desc', sortByField: 'netPrice',
    }, {
      sortBy: 'desc', sortByField: 'ratingTotal',
    }]

    const sortBy = this.getQueryParams().sortBy;
    const sortByField = this.getQueryParams().sortByField;

    if (sortBy && sortByField) {
      this.selectedSortOption = { sortBy, sortByField };
    } else {
      this.selectedSortOption = this.sortOptions[0];
    }
  }

  onSortOptionTouched(event: any = {}) {

    const option = Object.assign({}, event.detail.value);
    delete option.id;

    this.params = {
      ...this.params,
      ...option
    };

    this.onRefresh();

    this.navigateToRelative('.', option);
  }

  compareSortOption(o1: any, o2: any) {
    return o1 && o2 ? (o1.sortBy === o2.sortBy && o1.sortByField === o2.sortByField) : o1 === o2;
  };

  onRefresh(event: any = {}) {
    this.refresher = event.target;
    this.items = [];
    this.params.page = 0;
    this.loadData();
  }

  onContentLoaded() {
    setTimeout(() => {
      this.contentLoaded.next();
    }, 400);
  }

  receivedCartAddUpadte(item) {
    for (let _item of this.items) {
      console.log(_item);
      if (item.objectId === _item.objectId) {
        _item['existsInCart'] = true;
        _item['qty'] = item.qty;
      }
    }
  }

  receivedCartDeleteUpadte(item) {
    for (let _item of this.items) {
      console.log(_item);
      if (item.objectId === _item.objectId) {
        _item['existsInCart'] = false;
      }
    }
  }

  increment(item) {
    for (let _item of this.items) {
      if (item.objectId === _item.objectId) {
        _item['qty'] = item.qty;
      }
    }
  }

  receivedOrderUpadte(value) {
    for (let _item of this.items) {
      if (_item['existsInCart'] === true) {
        _item['existsInCart'] = value;
      }
    }
  }

  decrement(item) {
    for (let _item of this.items) {
      if (item.objectId === _item.objectId) {
        _item['qty'] = item.qty;
      }
    }
  }

  async loadData() {

    try {

      console.log(this.isComboItemList)

      let items = [];

      if (!this.isComboItemList) {
        items = await this.itemService.load(this.params);
      } else if (this.isComboItemList) {
        items = await this.comboItemService.load();
      }

      console.log(items.length);

      if (items.length !== 0) {
        let _items = [];

        for (const item of items) {
          console.log(item);
          _items.push(item);
        }

        this.items = _items;

        if (this.items.length) {
          this.showContentView();
        } else {
          this.showEmptyView();
        }

      } else {
        if (this.items.length === 0) {
          this.showEmptyView();
        }
      }

      this.onRefreshComplete(this.items);



      this.onRefreshComplete(this.items);
      this.onContentLoaded();

    } catch (error) {

      if (this.items.length) {
        this.showContentView();
      } else {
        this.showErrorView();
      }

      this.onRefreshComplete();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  async onSearch(event: any = {}) {

    const searchTerm = event.target.value;

    if (searchTerm) {

      try {
        this.items = await this.itemService.load({
          tag: searchTerm.toLowerCase(),
          limit: 10,
        });
      } catch (error) {
        console.log(error.message);
      }

    } else {
      this.loadData();
    }

  }

  async onSearchComboItem(event) {
    const searchTerm = event.target.value;

    if (searchTerm) {

      let items = [];

      try {
        items = await this.comboItemService.load({
          tag: searchTerm.toLowerCase(),
        });

        this.items = items;

      } catch (error) {
        console.log(error.message);
      }

      
    } else {
      this.loadData();
    }
  }

  async onClearSearch() {
    this.loadData();
  }

  onLoadMore(event: any = {}) {
    this.infiniteScroll = event.target;
    this.params.page++;
    this.loadData();
  }

  async onPresentFilterModal() {

    await this.showLoadingView({ showOverlay: true });

    const params = Object.assign({}, this.params);

    const allowed = [
      'sale',
      'featured',
      'ratingMin',
      'ratingMax',
      'priceMin',
      'priceMax',
      'cat',
      'brand',
    ];

    const filteredParams = Object.keys(params)
      .filter(key => allowed.includes(key))
      .reduce((obj, key) => {
        obj[key] = params[key]
        return obj
      }, {});

    const modal = await this.modalCtrl.create({
      component: FilterPage,
      componentProps: { params: filteredParams }
    });

    await modal.present();

    this.dismissLoadingView();

    const { data } = await modal.onDidDismiss();

    if (data) {

      const params = {
        ...this.params,
        ...data
      };

      this.params = params;

      this.showLoadingView({ showOverlay: false });
      this.onRefresh();

      this.navigateToRelative('.', data)
    }
  }

  onItemTouched(item: Item) {

    // for some reason the relative navigation isn't working
    // after updating the query params in the filter modal
    // so absolute nav is the workaround for now...

    // Get current url without params
    const url = this.router.url.split('?')[0];

    this.navigateTo(url + '/' + item.slug);
  }

  trackByFn(index: number, item: any) {
    if (!item) return null;
    return item.id;
  }

}
