import { Component, Injector, ViewChild } from '@angular/core';
import { IonSlides, IonContent } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { Slide } from '../../services/slide';
import { Item } from '../../services/item';
import * as Parse from 'parse';
import { Category } from '../../services/category';
import { SubCategory } from '../../services/sub-category';
import { Subject, Observable, merge } from 'rxjs';
import {
  trigger,
  style,
  animate,
  transition,
  query,
  stagger
} from '@angular/animations';
import { Brand } from 'src/app/services/brand';
import { ComboItem } from 'src/app/services/combo-item';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { SettingsPage } from '../settings-page/settings-page';
import { User } from 'src/app/services/user';
import { SignInPage } from '../sign-in/sign-in';
import { Notification } from 'src/app/services/notification';
import { LocationCheckService } from 'src/app/services/location-check.service';
import { Installation } from 'src/app/services/installation';
import { LocalStorage } from 'src/app/services/local-storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  styleUrls: ['home.scss'],
  animations: [
    trigger('staggerIn', [
      transition('* => *', [
        query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
        query(':enter', stagger('70ms', [animate('100ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
      ])
    ])
  ]
})
export class HomePage extends BasePage {

  @ViewChild(IonContent, { static: true }) content: IonContent;
  @ViewChild(IonSlides) ionSlides: IonSlides;

  public slidesConfig = {
    centeredSlides: true,
    slidesPerView: 1.1,
    spaceBetween: 7,
    grabCursor: true,
    initialSlide: 1,
    breakpointsInverse: true,
    loop: false,
    breakpoints: {
      992: {
        slidesPerView: 1.5,
        spaceBetween: 30,
      },
    }
  };

  public slidesBrandsConfig = {
    slidesOffsetBefore: 16,
    slidesOffsetAfter: 16,
    slidesPerView: 3.4,
    spaceBetween: 8,
    grabCursor: true,
  };

  public slidesItemsConfig = {
    slidesOffsetBefore: 16,
    slidesOffsetAfter: 16,
    slidesPerView: 2.4,
    spaceBetween: 16,
    grabCursor: true,
  };

  public skeletonArray = Array(6);

  public slides: Slide[] = [];
  public categories: Category[] = [];
  public itemsOnSale: Item[] = [];
  public itemsNewArrival: Item[] = [];
  public itemsFeatured: Item[] = [];
  public items: Item[] = [];
  public brands: Brand[] = [];
  public comboItems: ComboItem[] = [];

  public suggestions: Item[] = [];

  private queryItems: any = {};

  protected contentLoaded: Subject<any>;
  protected loadAndScroll: Observable<any>;

  public slidesBrandsEvent: Subject<any>;
  public slidesBrandsObservable: Observable<any>;

  public slidesItemsOnSaleEvent: Subject<any>;
  public slidesItemsOnSaleObservable: Observable<any>;

  public slidesItemsNewArrivalEvent: Subject<any>;
  public slidesItemsNewArrivalObservable: Observable<any>;

  public slidesItemsFeaturedEvent: Subject<any>;
  public slidesItemsFeaturedObservable: Observable<any>;

  public isSlidesLoaded: boolean;
  public isSlidesBrandsLoaded: boolean;

  public isSlidesItemsOnSaleLoaded: boolean;
  public isSlidesItemsNewArrivalLoaded: boolean;
  public isSlidesItemsFeaturedLoaded: boolean;

  constructor(injector: Injector,
    private subCategoryService: SubCategory,
    private itemService: Item,
    private comboItemService: ComboItem,
    private pubsub: NgxPubSubService,
    private user: User,
    private notifications: Notification,
    location: LocationCheckService,
    private installationService: Installation,
    private localStorage: LocalStorage,
    private categoryService: Category) {
    super(injector);
    pubsub.subscribe('tabs:openlink', (link) => {
      this.tabsopenlink(link);
    })

    pubsub.subscribe("cart:itemAdded", this.receivedCartAddUpadte.bind(this));
    pubsub.subscribe("cart:itemDeleted", this.receivedCartDeleteUpadte.bind(this));
    pubsub.subscribe("cart:itemIncrement", this.increment.bind(this));
    pubsub.subscribe("cart:itemDecrement", this.decrement.bind(this));
    pubsub.subscribe("cart:checkOut", this.receivedOrderUpadte.bind(this));

    location.isLocationEnabled();
  }

  receivedOrderUpadte(value) {
    for (let _item of this.items) {
      if (_item['existsInCart'] === true) {
        _item['existsInCart'] = value;
      }
    }
  }

  receivedCartAddUpadte(item) {
    for (let _item of this.items) {
      console.log(_item);
      if (item.objectId === _item.objectId) {
        _item['existsInCart'] = true;
        _item['qty'] = item.qty;
      }
    }
  }

  receivedCartDeleteUpadte(item) {
    for (let _item of this.items) {
      console.log(_item);
      if (item.objectId === _item.objectId) {
        _item['existsInCart'] = false;
      }
    }
  }

  increment(item) {
    for (let _item of this.items) {
      if (item.objectId === _item.objectId) {
        _item['qty'] = item.qty;
      }
    }
  }

  decrement(item) {
    for (let _item of this.items) {
      if (item.objectId === _item.objectId) {
        _item['qty'] = item.qty;
      }
    }
  }
  enableMenuSwipe(): boolean {
    return false;
  }

  tabsopenlink(link) {
    console.log(link);
    if (link == 'settings') {
      this.onPresentSettingsModal()
    } else {
      this.navigateTo('1/' + link);
    }

  }

  async ngOnInit() {
    this.setupObservable();
    this.loginAnomously();
    this.showLoadingView({ showOverlay: false });
    this.loadComboItems();
    this.loadItems();
    this.loadData();
  }

  onRefresh($event) {
    this.showLoadingView({ showOverlay: false });
    this.loadComboItems();
    this.loadItems();
    this.loadData($event);
  }

  async loginAnomously() {
    let user: User = User.getCurrent();

    if (!user) {
      let uid = Date.now();

      console.log("Crewating User");

      let user = new User;

      let _user = await user.signUpAnonymously(uid);

      console.log(_user);

      this.updateInstallation();
    }
  }

  async updateInstallation() {
    let user: User = User.getCurrent();

    let payload: any = {
      user: null
    };

    console.log("Here Is User", user);

    const id = await this.installationService.getId();

    const obj = await this.installationService.getOne(id);

    if (obj) {
      payload.isPushEnabled = obj.isPushEnabled;
      this.localStorage.setIsPushEnabled(obj.isPushEnabled);
      this.preference.isPushEnabled = obj.isPushEnabled;
    }

    if (user) {
      payload.user = user.toPointer();
    }

    const res = await this.installationService.save(id, payload);

    console.log("Here Is Payload", payload);
  }

  ionViewWillEnter() {
    if (this.content) {
      this.content.scrollToTop();
    }
    if (this.ionSlides) {
      this.ionSlides.startAutoplay();
    }
  }

  ionViewWillLeave() {
    if (this.ionSlides) {
      this.ionSlides.stopAutoplay();
    }
  }

  async ionViewDidEnter() {

    const title = await this.getTrans('APP_NAME');
    this.setPageTitle(title);

    this.loadItems();

    this.setMetaTags({
      title: title
    });
  }

  async onPresentSettingsModal() {

    if (!User.getCurrent()) return this.onPresentSignInModal();

    await this.showLoadingView({ showOverlay: true });

    const modal = await this.modalCtrl.create({
      component: SettingsPage
    });

    await modal.present();

    this.showContentView();
  }

  async onPresentSignInModal() {

    await this.showLoadingView({ showOverlay: true });

    const modal = await this.modalCtrl.create({
      component: SignInPage,
      componentProps: {
        showLoginForm: true
      }
    });

    await modal.present();

    this.showContentView();
  }

  onSlidesDidLoad() {
    this.isSlidesLoaded = true;
    this.ionSlides.startAutoplay();
  }

  onSlidesDidChange() {
    this.contentLoaded.next();
  }

  onSlidesBrandsChange() {
    this.slidesBrandsEvent.next();
  }

  onSlidesBrandsLoaded() {
    this.isSlidesBrandsLoaded = true;
  }

  onSlidesItemsOnSaleLoaded() {
    this.isSlidesItemsOnSaleLoaded = true;
  }

  onSlidesItemsNewArrivalLoaded() {
    this.isSlidesItemsNewArrivalLoaded = true;
  }

  onSlidesItemsFeaturedLoaded() {
    this.isSlidesItemsFeaturedLoaded = true;
  }

  onSlidesItemsOnSaleChange() {
    this.slidesItemsOnSaleEvent.next();
  }

  onSlidesItemsNewArrivalChange() {
    this.slidesItemsNewArrivalEvent.next();
  }

  onSlidesItemsFeaturedChange() {
    this.slidesItemsFeaturedEvent.next();
  }

  setupObservable() {

    this.contentLoaded = new Subject();

    this.loadAndScroll = merge(
      this.content.ionScroll,
      this.contentLoaded
    );

    this.slidesBrandsEvent = new Subject();

    this.slidesBrandsObservable = merge(
      this.content.ionScroll,
      this.slidesBrandsEvent,
    );

    this.slidesItemsOnSaleEvent = new Subject();

    this.slidesItemsOnSaleObservable = merge(
      this.content.ionScroll,
      this.slidesItemsOnSaleEvent,
    );

    this.slidesItemsNewArrivalEvent = new Subject();

    this.slidesItemsNewArrivalObservable = merge(
      this.content.ionScroll,
      this.slidesItemsNewArrivalEvent,
    );

    this.slidesItemsFeaturedEvent = new Subject();

    this.slidesItemsFeaturedObservable = merge(
      this.content.ionScroll,
      this.slidesItemsFeaturedEvent,
    );
  }

  onContentLoaded() {
    setTimeout(() => {
      this.contentLoaded.next();
    }, 400);
  }

  onSlideTouched(slide: Slide) {
    if (slide.item) {
      this.navigateToRelative('./items/' + slide.item.slug);
    } else if (slide.brand) {
      this.navigateToRelative('./items', {
        brand: slide.brand.id
      });
    } else if (slide.category) {
      this.navigateToRelative('./items', {
        cat: slide.category.id
      });
    } else if (slide.subcategory) {
      this.navigateToRelative('./items', {
        subcat: slide.subcategory.id
      });
    } else if (slide.url) {
      this.openUrl(slide.url);
    } else if (slide.type === "comboitemlist") {
      this.router.navigate(['1/home/items'], { queryParams: { "comboList": true }});
      // no action required
    }
  }

  async onCategoryTouched(category: Category) {

    try {

      if (category.subCategoryCount > 0) {

        this.navigateToRelative('./subcategories', {
          categoryId: category.id
        });

      } else if (category.subCategoryCount === 0) {

        this.navigateToRelative('./items', {
          cat: category.id
        });

      } else {

        await this.showLoadingView({ showOverlay: false });

        const count = await this.subCategoryService.count({
          category: category
        });

        if (count) {

          this.navigateToRelative('./subcategories', {
            categoryId: category.id
          });

        } else {

          this.navigateToRelative('./items', {
            cat: category.id
          });

        }

        this.showContentView();

      }

    } catch (error) {
      this.showContentView();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

  loadData(event: any = {}) {

    this.refresher = event.target;

    Parse.Cloud.run('getHomePageData').then(data => {

      this.slides = data.slides;
      this.categories = data.categories;
      this.itemsOnSale = data.itemsOnSale;
      this.itemsNewArrival = data.itemsNewArrival;
      this.itemsFeatured = data.itemsFeatured;
      this.brands = data.brands;
      if (this.content) {
        this.content.scrollToTop();
      }

      this.onRefreshComplete();
      this.showContentView();

      this.onContentLoaded();

    }, () => {
      this.onRefreshComplete();
      this.showErrorView();
    });

  }

  onLoadMore(event: any = {}) {
    this.infiniteScroll = event.target;
    this.queryItems.page++;
    this.loadItems();
  }

  async loadItems() {

    this.itemService.loadInCloud(this.queryItems).then((items: Item[]) => {

      for (let item of items) {

        let index = this.items.findIndex((x) => x.id === item.id);

        if (index === -1) {
          this.items.push(item);
        }

      }

      this.onRefreshComplete(items);

    }).catch(error => {
      console.warn(error);
    });

  }

  async loadComboItems() {
    this.comboItemService.loadInCloud().then((items: ComboItem[]) => {

      this.comboItems = [];
      for (let item of items) {
        this.comboItems.push(item);
        console.log(item.items);
        console.log(item.items.length);

      }

      console.log(this.comboItems);
    }).catch(error => {
      console.warn(error);
    });
  }

  async onClearSearch() {
    this.suggestions = [];
  }

    // async onClearCatSearch() {

  // }
  onBlurSearchInput() {
    this.suggestions = [];
  }

  async onSearch(event: any = {}) {



    const searchTerm = event.target.value;

    if (searchTerm) {

      try {

        this.items = await this.itemService.load({
          tag: searchTerm.toLowerCase(),
          limit: 10,
        });
      } catch (error) {
        console.log(error.message);
      }

    } else {
      this.items = await this.itemService.load({ limit: 10 });
    }

  }

  async onSearchCat(event: any = {}) {

    const searchTerm = event.target.value;

    if (searchTerm) {

      try {

        this.categories = await this.categoryService.load({
          tag: searchTerm.toLowerCase(),
          limit: 10,
        });
      } catch (error) {
        console.log(error.message);
      }

    } else {
      this.categories = await this.categoryService.load();
    }

  }

  onSubmitSearch(event: any = {}) {

    if (event.key === "Enter") {

      const searchTerm = event.target.value;

      if (searchTerm) {
        this.suggestions = [];
        this.navigateToRelative('./search', { q: searchTerm });
      }
    }
  }

  trackByFn(index: number, item: any) {
    if (!item) return null;
    return item.id;
  }

  private isPathFromHome(): boolean {
    return this.router.url === '/1/home/categories';
  }

  onViewAll(category) {
    const path = this.isPathFromHome() ? '../' : './';
    this.navigateToRelative(path + 'items', {cat: category.id});
  }

  onSuggestionTouched(suggestion: Item) {
    this.suggestions = [];
    const path = this.isPathFromHome() ? '../' : './';
    this.navigateToRelative(path + 'items/' + suggestion.slug);
  }
  

  async goToSubCategoryPage(category: Category) {

    const path = this.isPathFromHome() ? '../' : './';

    try {

      if (category.subCategoryCount > 0) {

        this.navigateToRelative(path + 'subcategories', {
          categoryId: category.id
        });

      } else if (category.subCategoryCount === 0) {

        this.navigateToRelative(path + 'items', {
          cat: category.id
        });

      } else {

        await this.showLoadingView({ showOverlay: false });

        const count = await this.subCategoryService.count({
          category: category
        });

        if (count) {

          this.navigateToRelative(path + 'subcategories', {
            categoryId: category.id
          });

        } else {

          this.navigateToRelative(path + 'items', {
            cat: category.id
          });

        }

        this.showContentView();

      }

    } catch (error) {
      this.showContentView();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }

  }

}