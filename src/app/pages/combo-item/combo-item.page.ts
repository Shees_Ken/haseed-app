import { Component, OnInit, Injector, ViewChild, NgZone } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { ComboItem } from 'src/app/services/combo-item';
import { GalleryConfig, Gallery } from '@ngx-gallery/core';
import { Subject, Observable, Subscription, merge } from 'rxjs';
import { IonSlides, IonContent } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Lightbox } from '@ngx-gallery/lightbox';
import { Item } from 'src/app/services/item';
import { Cart } from 'src/app/services/cart';
import { SharePage } from '../share/share.page';
import { User } from 'src/app/services/user';
import { SignInPage } from '../sign-in/sign-in';

@Component({
  selector: 'app-combo-item',
  templateUrl: './combo-item.page.html',
  styleUrls: ['./combo-item.page.scss'],
})
export class ComboItemPage extends BasePage implements OnInit {

  @ViewChild('slides') slides: IonSlides;
  @ViewChild(IonContent, { static: true }) content: IonContent;

  public comboItem: ComboItem;
  public comboItemPrice: number;
  public comboItems: Array<any> = [];

  public isLiked: any;

  public featuredComboItems: Array<ComboItem> = [];

  public qty: number;

  public isAddingToCart: boolean = false;

  public relatedSlidesConfig = {
    slidesPerView: 2.8,
    spaceBetween: 8,
    slidesOffsetBefore: 8,
    slidesOffsetAfter: 8,
    grabCursor: true,
    breakpointsInverse: true,
    breakpoints: {
      992: {
        slidesPerView: 4.2,
        spaceBetween: 16,
      },
    }
  };

  public webSocialShare: { show: boolean, share: any, onClosed: any } = {
    show: false,
    share: {
      config: [{
        facebook: {
          socialShareUrl: '',
        },
      }, {
        twitter: {
          socialShareUrl: '',
        }
      }, {
        whatsapp: {
          socialShareText: '',
          socialShareUrl: '',
        }
      }, {
        copy: {
          socialShareUrl: '',
        }
      }]
    },
    onClosed: () => {
      this.webSocialShare.show = false;
    }
  };

  constructor(
    injector: Injector,
    public comboItemService: ComboItem,
    private gallery: Gallery,
    private socialSharing: SocialSharing,
    private lightboxService: Lightbox,
    private zone: NgZone,
    private itemService: Item,
    private userService: User,
    private cartService: Cart,
  ) {
    super(injector)
    this.loadData();
  }

  public contentLoaded: Subject<any>;
  public loadAndScroll: Observable<any>;

  protected slidesEvent: Subject<any>;
  protected slidesObservable: Observable<any>;

  public slidesRelatedEvent: Subject<any>;
  public slidesRelatedObservable: Observable<any>;

  public isLightboxOpen: boolean;

  public lightboxSubscriptionOpen: Subscription;
  public lightboxSubscriptionClosed: Subscription;

  public backButtonListener: any;
  public isComboInCart: boolean = false;

  ngOnInit() {
    this.setupObservables();
    this.setupGallery();
    this.qty = 0;
  }


  enableMenuSwipe(): boolean {
    return true;
  }

  async checkifinCart() {
    
    console.log("Checking if item in cart");

    let cart = await this.cartService.getOne();

    if (cart) {
      const existInCart = cart.items.find((item: any) => {
        console.log(item);
        if (item.comboId !== undefined) {
          return item.comboId === this.comboItem.objectId
        }
      })
  
      if (existInCart) {
        this.isComboInCart = true;
      }
    }

  }

  setupGallery() {
    const config: GalleryConfig = {
      loadingMode: 'indeterminate'
    };

    const galleryRef = this.gallery.ref('itemGallery');
    galleryRef.setConfig(config);

    this.backButtonListener = (ev: CustomEvent) => {
      ev.detail.register(10, () => {

        this.zone.run(() => {
          if (this.isLightboxOpen) {
            this.lightboxService.close();
          } else {
            this.goBack();
          }
        });

      });
    };

    document.addEventListener('ionBackButton', this.backButtonListener);

    this.lightboxSubscriptionOpen = this.lightboxService.opened.subscribe(() => {
      this.isLightboxOpen = true;
    });
    this.lightboxSubscriptionClosed = this.lightboxService.closed.subscribe(() => {
      this.isLightboxOpen = false;
    });
  }

  async onShare() {

    const url = this.getShareUrl(this.comboItem.slug);

    if (this.isHybrid()) {

      try {
        await this.socialSharing.share(this.comboItem.name, null, null, url);
      } catch (err) {
        console.warn(err)
      }

    } else if (this.isPwa() || this.isMobile()) {

      this.webSocialShare.share.config.forEach((item: any) => {
        if (item.whatsapp) {
          item.whatsapp.socialShareUrl = url;
        } else if (item.facebook) {
          item.facebook.socialShareUrl = url;
        } else if (item.twitter) {
          item.twitter.socialShareUrl = url;
        } else if (item.copy) {
          item.copy.socialShareUrl = url;
        }
      });

      this.webSocialShare.show = true;
    } else {
      this.openShareModal(url);
    }

  }

  async openShareModal(url: string) {

    await this.showLoadingView({ showOverlay: true });

    const modal = await this.modalCtrl.create({
      component: SharePage,
      componentProps: { url }
    })

    await modal.present();

    this.showContentView();
  }

  setupObservables() {

    this.slidesEvent = new Subject();
    this.contentLoaded = new Subject();
    this.slidesRelatedEvent = new Subject();

    this.slidesObservable = merge(
      this.content.ionScroll,
      this.slidesEvent,
      this.contentLoaded,
    );

    this.slidesRelatedObservable = merge(
      this.content.ionScroll,
      this.slidesRelatedEvent,
      this.contentLoaded,
    );

    this.loadAndScroll = merge(
      this.content.ionScroll,
      this.contentLoaded,
    );
  }

  onContentLoaded() {
    setTimeout(() => {
      this.contentLoaded.next();
    }, 400);
  }

  async loadData() {
    try {

      if (this.comboItem) return;

      await this.showLoadingView({ showOverlay: false });

      const comboId = await this.getParams().comboId;

      this.comboItem = await this.comboItemService.loadOne(comboId);

      console.log(this.comboItem.items);
      this.comboItems = await this.filterComboItems(this.comboItem.items);

      console.log(this.comboItems.length);

      await this.calculateComboPrice(this.comboItems);

      this.featuredComboItems = await this.comboItemService.loadFeaturedComboItems();
      console.log(this.featuredComboItems);

      this.setPageTitle(this.comboItem.name);
      
      this.checkifinCart();

      this.setMetaTags({
        title: this.comboItem.name,
        image: this.comboItem.featuredImage ? this.comboItem.featuredImage.url() : '',
        slug: this.comboItem.slug
      });

      this.showContentView();
      this.checkIfItemIsLiked();

    } catch (error) {
      console.log(error);
      this.showErrorView();
    }

  }

  onSlidesDidLoad() {
    this.slidesEvent.next();
  }

  onSlidesWillChange() {
    this.slidesEvent.next();
  }

  onSlidesRelatedDrag() {
    this.slidesRelatedEvent.next();
  }

  calculateComboPrice(comboItems): Promise<number> {
    return new Promise(resolve => {
      let price = 0;

      comboItems.forEach(element => {
        let _price = element['salePrice'] ? element['salePrice']  : element['price']
        let itemPrice: number = _price * element['qty'];
        price = itemPrice + price;
      });
      this.comboItemPrice = price;
      resolve(price);
    })
  }

  async loadFeaturedItems() {
    this.featuredComboItems = await this.comboItemService.loadFeaturedComboItems();
    console.log(this.featuredComboItems);
  }

  async onAddToCart() {

    try {
      this.isAddingToCart = true;

      if (!User.getCurrent()) {
        const res = await this.userService.loginAnonymously();
        const user = await this.userService
          .becomeWithSessionToken(res.getSessionToken());
        window.dispatchEvent(new CustomEvent('user:login', { detail: user }));
      }

      let cart = await this.cartService.getOne();
      cart = cart || new Cart;

      const existInCart = cart.items.find((item: any) => {
        console.log(item);
        if (item.comboId !== undefined) {
          return item.comboId === this.comboItem.objectId
        }
      })

      if (existInCart) {
        this.isAddingToCart = false;
        return this.translate.get('ITEM_ALREADY_IN_CART')
          .subscribe(str => this.showToast(str));
      }

      for (let item of this.comboItems) {
        const rawItem = Object.assign({}, item.toJSON());

        const allowed: any = ['objectId'];

        const filteredItem: any = Object.keys(rawItem)
          .filter(key => allowed.includes(key))
          .reduce((obj, key) => {
            obj[key] = rawItem[key];
            obj['comboId'] = this.comboItem.objectId;
            console.log(this.comboItem.name);
            obj['comboName'] = this.comboItem.name;
            return obj;
          }, {});

        filteredItem.qty = item['qty'];

        cart.items.push(filteredItem);

      }
      this.isComboInCart = true;

      // let subtotal = cart.items.reduce((sum: number, item: any) => {
      //   return sum + item.amount;
      // }, 0);

      // console.log(subtotal);
      await cart.save();

      window.dispatchEvent(new CustomEvent('cart:updated', {
        detail: cart
      }));

      this.isAddingToCart = false;

      const { value } = await this.showSweetSuccessView();

      if (value) {
        this.setRoot('/1/cart/checkout');
      }

    } catch (err) {
      this.isAddingToCart = false;
      console.log(err);
      this.translate.get('ERROR_NETWORK').subscribe(str => this.showAlert(str));
    }
  }


  async incrementComboItemQuantity($event: any) {
    let _item = $event;
    console.log("console logging combo Items before Increment", this.comboItems);

    for (let item of this.comboItems) {
      if (_item.objectId === item.id) {
        _item.amount = _item.qty * (_item.salePrice || _item.price);
        item['qty'] = _item.qty;
      }
    }


    

    console.log("console loggging combo items after increment", this.comboItems);

    await this.calculateComboPrice(this.comboItems);

  }

  async decrementComboItemQuantity($event: any) {
    let _item = $event;

    for (let item of this.comboItems) {
      if (_item.objectId === item.id) {
        if (_item.qty > 0) {
          _item.amount = _item.qty * (_item.salePrice || _item.price);
          item['qty'] = _item.qty;
        } else {
          this.onRemoveItem(_item);
        } 
      }
    }

    await this.calculateComboPrice(this.comboItems);

  }

  async onRemoveItem(item: any) {

    try {

      let str = await this.getTrans('DELETE_CONFIRMATION');

      const res = await this.showConfirm(str);

      if (!res) return;

      await this.showLoadingView({ showOverlay: false });

      console.log(item);

      for (let _item of this.comboItems) {
        console.log(item);
        
        if (item.objectId === _item.id) {
          let index: number = this.comboItems.indexOf(item);
          console.log(index);
          if (index !== -1) {
            this.comboItems.splice(index, 1);
          }
        }
      }

      await this.calculateComboPrice(this.comboItems);

      this.showContentView();

    } catch (error) {
      this.showContentView();
    }

  }

  isQuantityInvalid() {
    return this.qty == 0;
  }

  goToComboItemPage(slug) {
    console.log(slug);
    this.navigateTo('1/home/combo-items/' + slug);
  }


  filterComboItems(_comboItem): Promise<any[]> {
    return new Promise(resolve => {
      let __comboItems = [];

      for (let i = 0; i < _comboItem.length; i++) {
        const element = _comboItem[i];
        console.log(element['item_id']);
        this.itemService.loadOne(element['item_id']).then(comboItem_item => {
          comboItem_item['qty'] = element['quantity'];
          comboItem_item['amount'] = comboItem_item['qty'] * (comboItem_item.salePrice || comboItem_item.price);
          console.log(comboItem_item);
          __comboItems.push(comboItem_item);
          if (i == _comboItem.length - 1) {
            console.log("The Combo Items ahve been finished")
            console.log(__comboItems);
            console.log(__comboItems.length);

            resolve(__comboItems)
          }
        });
      }

    })

  }

  async onLike() {

    if (User.getCurrent()) {

      try {
        this.isLiked = !this.isLiked;
        await this.comboItemService.like(this.comboItem.id);
      } catch (error) {
        console.log(error.message);
      }

    } else {
      this.presentSignInModal();
    }

  }

  async checkIfItemIsLiked() {

    if (User.getCurrent()) {

      try {
        this.isLiked = await this.comboItemService.isLiked(this.comboItem.id);
      } catch (error) {
        console.warn(error.message);
      }

    }
  }


  async presentSignInModal() {

    await this.showLoadingView({ showOverlay: true });

    const modal = await this.modalCtrl.create({
      component: SignInPage,
      componentProps: {
        showLoginForm: true
      }
    });

    await modal.present();

    this.showContentView();
  }
}
