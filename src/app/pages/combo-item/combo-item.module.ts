import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ComboItemPage } from './combo-item.page';
import { SharedModule } from 'src/app/shared.module';
import { GalleryModule } from '@ngx-gallery/core';
import { LightboxModule } from '@ngx-gallery/lightbox';
import { GallerizeModule } from '@ngx-gallery/gallerize';
import { SharePageModule } from '../share/share.module';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ComboItemPage
      }
    ]),
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    GalleryModule,
    LightboxModule,
    GallerizeModule,
    SharePageModule,
  ],
  declarations: [ComboItemPage]
})
export class ComboItemPageModule {}
