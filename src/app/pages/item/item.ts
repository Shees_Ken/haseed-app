import { Component, Injector, ViewChild, NgZone } from '@angular/core';
import { IonSlides, IonContent } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { Item } from '../../services/item';
import { Cart } from '../../services/cart';
import { User } from '../../services/user';
import { SignInPage } from '../sign-in/sign-in';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { SharePage } from '../share/share.page';
import { Subject, Observable, merge, Subscription } from 'rxjs';
import { Review } from 'src/app/services/review';
import { ItemVariation } from 'src/app/services/item-variation';
import { GalleryConfig, Gallery } from '@ngx-gallery/core';
import { Lightbox } from '@ngx-gallery/lightbox';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';

@Component({
  selector: 'page-item',
  templateUrl: './item.html',
  styleUrls: ['./item.scss']
})
export class ItemPage extends BasePage {

  @ViewChild('slides') slides: IonSlides;
  @ViewChild(IonContent, { static: true }) content: IonContent;

  public item: Item;
  public itemDescription: any;
  public isAddingToCart: boolean = false;
  public isLiked: boolean = false;
  public isInCart: boolean = false;

  public variations: ItemVariation[] = [];
  public selectedVariation: ItemVariation;

  public qty: number;

  public reviews: Review[] = [];
  public featuredItems: Item[] = [];

  public relatedSlidesConfig = {
    slidesPerView: 2.8,
    spaceBetween: 8,
    slidesOffsetBefore: 8,
    slidesOffsetAfter: 8,
    grabCursor: true,
    breakpointsInverse: true,
    breakpoints: {
      992: {
        slidesPerView: 4.2,
        spaceBetween: 16,
      },
    }
  };

  public webSocialShare: { show: boolean, share: any, onClosed: any } = {
    show: false,
    share: {
      config: [{
        facebook: {
          socialShareUrl: '',
        },
      }, {
        twitter: {
          socialShareUrl: '',
        }
      }, {
        whatsapp: {
          socialShareText: '',
          socialShareUrl: '',
        }
      }, {
        copy: {
          socialShareUrl: '',
        }
      }]
    },
    onClosed: () => {
      this.webSocialShare.show = false;
    }
  };

  public contentLoaded: Subject<any>;
  public loadAndScroll: Observable<any>;

  protected slidesEvent: Subject<any>;
  protected slidesObservable: Observable<any>;

  public slidesRelatedEvent: Subject<any>;
  public slidesRelatedObservable: Observable<any>;

  public isLightboxOpen: boolean;

  public lightboxSubscriptionOpen: Subscription;
  public lightboxSubscriptionClosed: Subscription;

  public backButtonListener: any;

  constructor(injector: Injector,
    private gallery: Gallery,
    private socialSharing: SocialSharing,
    private lightboxService: Lightbox,
    private itemService: Item,
    private userService: User,
    private reviewService: Review,
    private zone: NgZone,
    private cartService: Cart,
    private pubsub: NgxPubSubService) {
    super(injector);
    this.qty = 1;
  }

  enableMenuSwipe(): boolean {
    return false;
  }

  ngOnInit() {
    this.setupObservables();
    this.setupGallery();
    this.loadFeaturedItems();
  }


  ngOnDestroy() {
    this.lightboxSubscriptionOpen.unsubscribe();
    this.lightboxSubscriptionClosed.unsubscribe();
    document.removeEventListener('ionBackButton', this.backButtonListener);
  }

  async checkIfItemInCart() {

    try {

      let cart = await this.cartService.getOne();

      if (cart) {

        const existInCart = cart.items.filter(x => {
          if (!x.comboId) {
            return x.objectId == this.item.objectId;
          }
        });

        if (existInCart.length == 0) {
          this.isInCart = false;
        } else if (existInCart) {
          this.qty = existInCart[0]['qty'];
          this.isInCart = true;
        }

      }

    } catch (err) {
      console.error(err);
    }

  }

  setupGallery() {
    const config: GalleryConfig = {
      loadingMode: 'indeterminate'
    };

    const galleryRef = this.gallery.ref('itemGallery');
    galleryRef.setConfig(config);

    this.backButtonListener = (ev: CustomEvent) => {
      ev.detail.register(10, () => {

        this.zone.run(() => {
          if (this.isLightboxOpen) {
            this.lightboxService.close();
          } else {
            this.goBack();
          }
        });

      });
    };

    document.addEventListener('ionBackButton', this.backButtonListener);

    this.lightboxSubscriptionOpen = this.lightboxService.opened.subscribe(() => {
      this.isLightboxOpen = true;
    });
    this.lightboxSubscriptionClosed = this.lightboxService.closed.subscribe(() => {
      this.isLightboxOpen = false;
    });
  }

  setupObservables() {

    this.slidesEvent = new Subject();
    this.contentLoaded = new Subject();
    this.slidesRelatedEvent = new Subject();

    this.slidesObservable = merge(
      this.content.ionScroll,
      this.slidesEvent,
      this.contentLoaded,
    );

    this.slidesRelatedObservable = merge(
      this.content.ionScroll,
      this.slidesRelatedEvent,
      this.contentLoaded,
    );

    this.loadAndScroll = merge(
      this.content.ionScroll,
      this.contentLoaded,
    );
  }

  onContentLoaded() {
    setTimeout(() => {
      this.contentLoaded.next();
    }, 400);
  }

  async ionViewDidEnter() {

    try {

      if (this.item) return;

      await this.showLoadingView({ showOverlay: false });

      const itemId = await this.getParams().itemId;
      this.item = await this.itemService.loadOne(itemId);
      console.log(this.item);

      if (this.item.hasVariations()) {
        this.variations = this.item.variations
          .filter(variation => variation.isActive)
      }

      this.checkIfItemInCart();
      this.loadReviews();

      if (this.item.description) {
        this.itemDescription = this.sanitizer.bypassSecurityTrustHtml(this.item.description);
      }

      this.setPageTitle(this.item.name);

      this.setMetaTags({
        title: this.item.name,
        description: this.item.description,
        image: this.item.featuredImage ? this.item.featuredImage.url() : '',
        slug: this.item.slug
      });

      this.showContentView();
      this.onContentLoaded();
      this.checkIfItemIsLiked();
      this.trackView();

    } catch (error) {
      this.showErrorView();
    }

  }

  onSlidesDidLoad() {
    this.slidesEvent.next();
  }

  onSlidesWillChange() {
    this.slidesEvent.next();
  }

  onSlidesRelatedDrag() {
    this.slidesRelatedEvent.next();
  }

  async loadReviews() {
    try {
      this.reviews = await this.reviewService.load({
        item: this.item, limit: 10
      });
      this.onContentLoaded();
    } catch (err) {
      console.warn(err.message);
    }
  }

  async presentSignInModal() {

    await this.showLoadingView({ showOverlay: true });

    const modal = await this.modalCtrl.create({
      component: SignInPage,
      componentProps: {
        showLoginForm: true
      }
    });

    await modal.present();

    this.showContentView();
  }

  async checkIfItemIsLiked() {

    if (User.getCurrent()) {

      try {
        this.isLiked = await this.itemService.isLiked(this.item.id);
      } catch (error) {
        console.warn(error.message);
      }

    }
  }

  async trackView() {
    try {
      await this.itemService.trackView(this.item.id);
    } catch (error) {
      console.warn(error.message);
    }
  }

  async onShare() {

    const url = this.getShareUrl(this.item.slug);

    if (this.isHybrid()) {

      try {
        await this.socialSharing.share(this.item.name, null, null, url);
      } catch (err) {
        console.warn(err)
      }

    } else if (this.isPwa() || this.isMobile()) {

      this.webSocialShare.share.config.forEach((item: any) => {
        if (item.whatsapp) {
          item.whatsapp.socialShareUrl = url;
        } else if (item.facebook) {
          item.facebook.socialShareUrl = url;
        } else if (item.twitter) {
          item.twitter.socialShareUrl = url;
        } else if (item.copy) {
          item.copy.socialShareUrl = url;
        }
      });

      this.webSocialShare.show = true;
    } else {
      this.openShareModal(url);
    }

  }

  async openShareModal(url: string) {

    await this.showLoadingView({ showOverlay: true });

    const modal = await this.modalCtrl.create({
      component: SharePage,
      componentProps: { url }
    })

    await modal.present();

    this.showContentView();
  }

  async onLike() {

    if (User.getCurrent()) {

      try {
        this.isLiked = !this.isLiked;
        await this.itemService.like(this.item.id);
      } catch (error) {
        console.log(error.message);
      }

    } else {
      this.presentSignInModal();
    }

  }

  onVariationTouched(variation: ItemVariation) {
    this.selectedVariation = variation;
  }

  isQuantityInvalid() {
    return this.qty == 0;
  }

  inc() {
    this.qty++;
  }

  dec() {
    this.qty--;
  }

  async onAddToCart() {

    try {

      if (this.item.hasVariations() && !this.selectedVariation) {
        return this.translate.get('CHOOSE_VARIATION').subscribe(str => this.showToast(str));
      }

      this.isAddingToCart = true;

      if (!User.getCurrent()) {
        const res = await this.userService.loginAnonymously();
        const user = await this.userService
          .becomeWithSessionToken(res.getSessionToken());
        window.dispatchEvent(new CustomEvent('user:login', { detail: user }));
      }

      const rawItem = Object.assign({}, this.item.toJSON());

      const allowed: any = ['objectId'];

      const filteredItem: any = Object.keys(rawItem)
        .filter(key => allowed.includes(key))
        .reduce((obj, key) => {
          obj[key] = rawItem[key];
          return obj;
        }, {});

      filteredItem.qty = this.qty;

      console.log(filteredItem);

      if (this.selectedVariation) {
        filteredItem.variation = this.selectedVariation.toJSON();
      }

      let cart = await this.cartService.getOne();
      cart = cart || new Cart;

      console.log(cart.items);
      const existInCart = cart.items.find((item: any) => {
        if (!item.comboId) {
          if (item.variation) {
            return item.objectId === filteredItem.objectId && item.variation.objectId === filteredItem.variation.objectId;
          }
          return item.objectId === filteredItem.objectId
        }
      })

      if (existInCart) {
        this.isAddingToCart = false;
        return this.translate.get('ITEM_ALREADY_IN_CART')
          .subscribe(str => this.showToast(str));
      }

      console.log(filteredItem);
      cart.items.push(filteredItem);

      await cart.save();

      this.item['qty'] = this.qty;
      this.pubsub.publishEvent('cart:itemAdded', this.item);

      this.isInCart = true;

      window.dispatchEvent(new CustomEvent('cart:updated', {
        detail: cart
      }));

      this.isAddingToCart = false;

      const { value } = await this.showSweetSuccessView();

      if (value) {
        this.setRoot('/1/cart/checkout');
      }

    } catch (err) {
      this.isAddingToCart = false;
      this.translate.get('ERROR_NETWORK').subscribe(str => this.showAlert(str));
    }
  }

  async incCartItemQuantity() {
    this.qty++;
    console.log(this.qty);


    let cart = await this.cartService.getOne();

    cart.items = cart.items.map((x) => {
      if (!x.comboId) {
        if (x.objectId === this.item.objectId) {
          x.qty = this.qty;
          x.amount = x.qty * (x.salePrice || x.price);
        }
      }
      return x;
    });

    this.preference.subtotal = cart.items.reduce((sum: number, item: any) => {
      return sum + item.amount;
    }, 0);
    this.item["qty"] = this.qty;
    this.pubsub.publishEvent("cart:itemIncrement", this.item);
  }

  async decCartItemQuantity() {
    if (this.qty > 0) {
      this.qty--;

      let cart = await this.cartService.getOne();

      cart.items = cart.items.map((x) => {
        if (!x.comboId) {
          if (x.objectId === this.item.objectId) {
            if (x.qty > 0) {
              x.qty = this.qty;
              x.amount = x.qty * (x.salePrice || x.price);
            }
          }
        }
        return x;
      });

      this.preference.subtotal = cart.items.reduce((sum: number, item: any) => {
        return sum + item.amount;
      }, 0);
      this.item["qty"] = this.qty;
      this.pubsub.publishEvent("cart:itemDecrement", this.item);
    }
  }

  trackByFn(index: number, item: any) {
    if (!item) return null;
    return item.id;
  }

  async loadFeaturedItems() {
    this.featuredItems = await this.itemService.loadFeaturedItems();
    console.log(this.featuredItems);
  }

  goToItemPage(slug) {
    console.log(slug);
    this.navigateTo('1/home/items/' + slug);
  }
}
