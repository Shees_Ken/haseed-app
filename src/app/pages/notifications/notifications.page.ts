import { Component, Injector, OnInit } from '@angular/core';
import { Notification } from 'src/app/services/notification';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage extends BasePage implements OnInit {

  public notifications: Notification[];

  constructor(
    injector: Injector,
    private notification: Notification
  ) {
    super(injector)
  }

  enableMenuSwipe() {
    return false;
  }

  ngOnInit() {
    this.loadData();
  }

  async ionViewDidEnter() {

    const title = await this.getTrans('NOTIFICATIONS');
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });
  }

  async loadData() {
    try {
      console.log("I am getting the noptifocatios");
      this.notifications = await this.notification.load();

      console.log(this.notifications);
      
      if (this.notifications.length > 0) {
        this.onRefreshComplete();
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (err) {
      console.error(err);
      console.error("I am getting the noptifocatios");

      this.showErrorView();
      this.translate.get('ERROR_NETWORK').subscribe((str) => this.showToast(str));
    }
  }


  goToNotificationPage(notification) {
    console.log(notification);

    let page;
    let queryParams;

    if (notification.brand) {
      page = '/1/home/items';
      queryParams = { brand: notification.brand.id };
    } else if (notification.category) {
      page = '/1/home/items';
      queryParams = { cat: notification.category.id };
    } else if (notification.subcategory) {
      page = '/1/home/items';
      queryParams = { subcat: notification.subcategory.id };
    } else if (notification.item.id) {
      page = '/1/home/items/' + notification.item.id;
      queryParams = {};
    }

    this.router.navigate([page], { queryParams });

  }
  
}
