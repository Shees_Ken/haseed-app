import { Component, Injector, NgZone, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { Cart } from '../../services/cart';
import { User } from '../../services/user';
import { Subject, Observable, merge } from 'rxjs';
import * as _ from 'underscore';
import { ComboItem } from 'src/app/services/combo-item';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { SignInPage } from '../sign-in/sign-in';

@Component({
  selector: 'cart-page',
  templateUrl: './cart-page.html',
  styleUrls: ['./cart-page.scss']
})
export class CartPage extends BasePage {

  @ViewChild(IonContent, { static: true }) content: IonContent;

  public cart: Cart;
  public isSavingCart: boolean;
  public comboItems: any;
  public items: any;

  protected contentLoaded: Subject<any>;
  protected loadAndScroll: Observable<any>;

  public subtotal = 0;
  public _subtotal;

  constructor(injector: Injector,
    private cartService: Cart,
    private _ngZone: NgZone,
    private comboItemService: ComboItem,
    private pubsub: NgxPubSubService) {
    super(injector);

    window.addEventListener('user:loggedOut', () => {
      this.cart = null;
      this.showEmptyView();
    });

    window.addEventListener('user:login', () => {
      this.loadData();
    });

    this.contentLoaded = new Subject();

  }

  enableMenuSwipe(): boolean {
    return true;
  }


  ngOnInit() {
    this.setupObservable();
  }

  ionViewWillEnter() {
    if (this.content) {
      this.content.scrollToTop();
    }
  }

  async ionViewDidEnter() {
    const user = User.getCurrent()

    console.log(user);

    // if (!user) {
    //   this.navigateTo("1/home");
    //   this.onPresentSignInModal();
    // } else {
    this.showLoadingView({ showOverlay: false });
    this.loadData();
    // }


    const title = await this.getTrans('CART');
    this.setPageTitle(title);

    this.setMetaTags({
      title: title
    });
  }

  async onPresentSignInModal() {

    await this.showLoadingView({ showOverlay: true });

    const modal = await this.modalCtrl.create({
      component: SignInPage,
      componentProps: {
        showLoginForm: true,
        isDismissible: true,
      },
      // backdropDismiss:false
    });

    await modal.present();

    this.showContentView();
  }

  setupObservable() {
    this.loadAndScroll = merge(
      this.content.ionScroll,
      this.contentLoaded
    );
  }

  onContentLoaded() {
    setTimeout(() => {
      this.contentLoaded.next();
    }, 400);
  }

  async loadData(event: any = {}) {

    try {

      this.refresher = event.target;

      this.cart = await this.cartService.getOne();

      if (this.cart) {

        console.log(this.cart.items);
        this.items = this.cart.items.filter((x: any) => !("comboId" in x))

        console.log(this.items);

        let combo_Items = this.cart.items.filter((x: any) => ("comboId" in x))

        this.comboItems = _.groupBy(combo_Items, 'comboName');

        console.log(this.comboItems);

        if (this.cart && !this.cart.empty()) {
          this.showContentView();
          this._subtotal = this.preference.subtotal;
          console.log(this._subtotal);
          if (this._subtotal !== this.cart.subtotal && this._subtotal !== undefined) {
            console.log(this.cart.subtotal);
            if (this._subtotal <= this.cart.subtotal) {
              this.subtotal = this.cart.subtotal;
            } else {
              this.subtotal = this._subtotal;
            }
          } else {
            this.subtotal = this.cart.subtotal;
          }

        } else {
          this.showEmptyView();
        }

        if (this.cart && this.cart.status === 'expired') {
          this.onCartExpired(this.cart);
        }

        this.onContentLoaded();

        this.onRefreshComplete(this.cart);
      } else {
        this.showEmptyView();
      }


    } catch (error) {
      this.showErrorView();
      this.translate.get('ERROR_NETWORK').subscribe(str => this.showToast(str));
    }
  }

  incrementQuantity(item: any) {
    this.cart.items = this.cart.items.map((x) => {
      if (item.comboId) {
        if (item.comboId === x.comboId) {
          if (x.objectId === item.objectId) {
            x.qty = item.qty;
            x.amount = x.qty * (x.salePrice || x.price);
            item.amount = x.amount;
            this.pubsub.publishEvent("cart:itemIncrement", item);
            this.subtotal = this.cart.calculateSubtotal();
            console.log(this.subtotal);
          }
        }
      } else {
        if (!x.comboId) {
          if (x.objectId === item.objectId) {
            console.log("calling this");
            x.qty = item.qty;
            x.amount = x.qty * (x.salePrice || x.price);
            item.amount = x.amount;
            this.pubsub.publishEvent("cart:itemIncrement", item);
            this.subtotal = this.cart.calculateSubtotal();
            console.log(this.subtotal);
          }
        }
      }

      return x;
    });

    this.cart.set("subtotal", this.subtotal);
    this.cart.save();
    this.preference.subtotal = undefined;
  }

  decrementQuantity(item: any) {
    console.log(item);
    this.cart.items = this.cart.items.map((x) => {
      if (item.comboId) {
        if (item.comboId === x.comboId) {
          if (x.objectId === item.objectId) {
            console.log("calling this for comboItem");
            x.qty = item.qty;
            if (x.qty > 0) {
              x.amount = x.qty * (x.salePrice || x.price);
              item.amount = x.amount;
              this.pubsub.publishEvent("cart:itemDecrement", item);
              this.subtotal = this.cart.calculateSubtotal();
              console.log(this.subtotal);
            } else {
              this.onRemoveItem(item);
            }
          }
        }
      } else {
        if (!x.comboId) {
          if (x.objectId === item.objectId) {
            console.log("calling this");
            x.qty = item.qty;
            if (x.qty > 0) {
              x.amount = x.qty * (x.salePrice || x.price);
              item.amount = x.amount;
              this.pubsub.publishEvent("cart:itemDecrement", item);
              this.subtotal = this.cart.calculateSubtotal();
              console.log(this.subtotal);
            } else {
              this.onRemoveItem(item);
            }
          }
        }
      }

      return x;
    });

    this.cart.set("subtotal", this.subtotal);
    this.cart.save();
    this.preference.subtotal = undefined;

  }

  onItemTouched(item: any) {
    this.navigateToRelative('./items/' + item.objectId + '/' + item.slug);
  }

  async onRemoveItem(item: any) {

    try {

      let str = await this.getTrans('DELETE_CONFIRMATION');

      const res = await this.showConfirm(str);

      if (!res) return;
      var self = this;
      let objectId = item.objectId;
      let comboId = item.comboId;

      console.log(item);

      if (objectId) {
        this._ngZone.runOutsideAngular(async () => {

          self.cart.items.forEach(x => {
            if (x.comboId) {
              if (comboId === x.comboId) {
                console.log(x);
                if (objectId === x.objectId) {
                  let index = self.cart.items.indexOf(x);
                  console.log("From Combo Items" ,index);
                  self.cart.items.splice(index, 1);
                }
              }
            } else if (!x.comboId) {
              if (objectId === x.objectId) {
                let index = self.cart.items.indexOf(x);
                console.log("From Items" ,index);
                self.cart.items.splice(index, 1);
                console.log(index);
              }
            }
          });



          await self.cart.save();

          if (self.cart.empty()) {
            self.subtotal = 0;
            self.showEmptyView();
          } else {
            self.subtotal = self.cart.calculateSubtotal();
            this.showContentView();
          }

          // reenter the Angular zone and display done
          this._ngZone.run(async () => {
            await self.loadData();
            console.log('Outside Done!');
          });
        });
      }

      window.dispatchEvent(new CustomEvent('cart:updated', {
        detail: self.cart
      }));

    } catch (error) {
      console.log(error);
      this.showContentView();
    }

  }

  onCartExpired(cart: Cart) {
    this.subtotal = 0;
    this.showEmptyView();
    window.dispatchEvent(new CustomEvent('cart:expired', {
      detail: cart
    }));
  }

  async goToCheckout() {

    try {

      // console.log(this.cart.dirty());

      // if (this.cart.dirty()) {
      //   this.isSavingCart = true;
      //   // await this.cart.save();
      //   this.isSavingCart = false;
      // }

      if (this.cart.status === 'expired') {
        this.onCartExpired(this.cart);
      } else {
        this.navigateToRelative('./checkout');

      }


    } catch (error) {
      console.log(error);
      this.isSavingCart = false;
      this.translate.get('ERROR_NETWORK').subscribe(str => this.showToast(str));
    }

  }

  async onDeleteComboItem(item) {

    console.log(item);
    let str = await this.getTrans('DELETE_CONFIRMATION');
    const res = await this.showConfirm(str);
    if (!res) return;

    // let comboItems = this.cart.items.filter((x) => x.comboName === comboName);
    // console.log(comboItems);

    var self = this;
    let firstId = item.value.length > 0 ? item.value[0]["comboId"] : null;
    if (firstId) {
      this._ngZone.runOutsideAngular(async () => {
        self.cart.items = self.cart.items.filter((x) => {
          return x.comboId != firstId
        })

        await self.cart.save();

        if (self.cart.empty()) {
          self.subtotal = 0;
          self.showEmptyView();
        } else {
          self.subtotal = self.cart.calculateSubtotal();
          this.showContentView();
        }

        // reenter the Angular zone and display done
        this._ngZone.run(async () => {
          await self.loadData();
          console.log('Outside Done!');
        });
      });
    }





    // for (let item of comboItems) {
    //   await this.showLoadingView({ showOverlay: false });

    //   console.log(item);

    //   let index: number = this.cart.items.indexOf(item);
    //   if (index !== -1) {
    //     this.cart.items.splice(index, 1);
    //   }

    //   await this.cart.save();

    window.dispatchEvent(new CustomEvent('cart:updated', {
      detail: this.cart
    }));
    // }
  }

  ionViewDidLeave() {
    this.cart.set("subtotal", this.subtotal);
    this.cart.save();
    this.preference.subtotal = undefined;
  }

}
