import { Pipe, LOCALE_ID, Inject } from "@angular/core";
import { CurrencyPipe } from "@angular/common";
import { environment } from 'src/environments/environment';
import { Preference } from '../services/preference';

@Pipe({
  name: "currencyGlobal"
})
export class CurrencyGlobalPipe extends CurrencyPipe {
  public exhange_rate = 1
  constructor(
    @Inject(LOCALE_ID) public locale: string,
    private preference: Preference) {
    super(locale);
  }

  transform(value: number): any {
    
    const locale = this.preference.lang || environment.defaultLang;
    const currency = locale == 'en' ? 'IQD' : 'د.ع';

    return this.iraqiRate(value, locale);
    // return super.transform(
    //   value,
    //   environment.currency.code,
    //   environment.currency.display,
    //   environment.currency.digitsInfo,
    //   locale
    // );
  }

  public setExchangeRate(){
    this.exhange_rate = 1;
  }

  public iraqiRate(price, locale = 'ar'){
    //  | currency:'د.ع'
    const num = `${this.formatNumber(Math.round(price * this.exhange_rate))} `;
    return locale == 'ar' ? `${num}` + this.getCurrency() : this.getCurrency() +`${num}`; 

  }

  public iraqiFormat(price){
    //  | currency:'د.ع'
    return `${this.formatNumber(Math.round(price))}`;
  }

  formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  discountFormatString(price){
    console.log(price);
    let p = (price * 100).toFixed(0)
    return ` ${p}%`;
  }

  getCurrency(){
    return this.preference.lang == 'en' ? 'IQD' : 'د.ع';
  }
}
