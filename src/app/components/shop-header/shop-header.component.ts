import { Component, OnInit, Input, Output, EventEmitter, Injector } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Preference } from 'src/app/services/preference';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-shop-header',
  templateUrl: './shop-header.component.html',
  styleUrls: ['./shop-header.component.scss'],
})
export class ShopHeaderComponent extends BasePage implements OnInit{
  
  @Input() pageTitle = '';
  @Input() showMenu = false;
  @Input() showSearch = false;
  @Input() showCart = false;
  @Input() isLiked = false;
  @Input() showLike = false;
  @Input() canGoBack = false;
  @Input() showLogo = false;

  @Output('onLike') onLike: EventEmitter<any> = new EventEmitter<any>();
  @Output('search') search: EventEmitter<any> = new EventEmitter<any>();
  
  public isSearchOn = false;

  constructor(
    injector: Injector,
    public Nav: NavController,
    public router: Router,
    public preference: Preference,
    public pubsub: NgxPubSubService
  ) {
    super(injector)
   }

  enableMenuSwipe(): boolean {
    return true;
  }

  ngOnInit() {}

  // SearchBy($event){
  //   $event.stopPropagation();
  //   let value = $event.target.value
  //   console.log(value);
  //   if(!value) return 
  //   this.search.emit(value)
  // }
  
  goToItemPage($event) {
    this.navigateTo('1/home/items');
    this.pubsub.publishEvent('product:searchOn', {$event});
  }
  openTab(link) {
    this.pubsub.publishEvent('openLink', link);
  }

  goback(){
    this.Nav.back();
  }

}
