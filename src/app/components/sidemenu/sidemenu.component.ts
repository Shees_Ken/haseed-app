import { Component, OnInit } from '@angular/core';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
})
export class SidemenuComponent implements OnInit {

  constructor(
    private pubsub: NgxPubSubService
  ) { }

  ngOnInit() { }


  openTab(link) {
    this.pubsub.publishEvent('sidemenu:openLink', link);
  }
}
