import { Component, OnInit, Input, Injector } from '@angular/core';
import { Item } from 'src/app/services/item';
import { Observable } from 'rxjs';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { Cart } from 'src/app/services/cart';
import { User } from 'src/app/services/user';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';

@Component({
  selector: 'app-shop-item',
  templateUrl: './shop-item.component.html',
  styleUrls: ['./shop-item.component.scss'],
})
export class ShopItemComponent extends BasePage implements OnInit {

  _item: any;

  get item(): any {
    return this._item;
  }

  @Input('item') set item(value: any) {
    this._item = value;
    this.checkIfItemInCart(value);
  };

  @Input() showCartIcon = true;
  @Input() showRadius = false;
  // @Input() showEmptyBlock = true;
  @Input('customObservable') scrollObservable: Observable<any>;

  public isAddingToCart: boolean;
  public qty: number;


  constructor(
    injector: Injector,
    private cartService: Cart,
    private userService: User,
    private pubsub: NgxPubSubService
  ) {
    super(injector);
    this.qty = 1;
  }

  enableMenuSwipe() {
    return false;
  }

  ngOnInit() { }

  async addToCart($event, item) {

    $event.stopPropagation();
    console.log(item);

    try {

      this.isAddingToCart = true;
      const user = User.getCurrent()

      console.log(user);

      if (!user) {
        const res = await this.userService.loginAnonymously();
        const user = await this.userService
          .becomeWithSessionToken(res.getSessionToken());
        window.dispatchEvent(new CustomEvent('user:login', { detail: user }));
      }


      const rawItem = Object.assign({}, this.item.toJSON());

      const allowed: any = ['objectId'];

      const filteredItem: any = Object.keys(rawItem)
        .filter(key => allowed.includes(key))
        .reduce((obj, key) => {
          obj[key] = rawItem[key];
          return obj;
        }, {});

      filteredItem.qty = this.qty;

      let cart = await this.cartService.getOne();
      cart = cart || new Cart;

      console.log(cart.items);
      const existInCart = cart.items.find((item: any) => {
        if (!item.comboId) {
          if (item.variation) {
            return item.objectId === filteredItem.objectId && item.variation.objectId === filteredItem.variation.objectId;
          }
          return item.objectId === filteredItem.objectId
        }
      })

      if (existInCart) {
        this.isAddingToCart = false;
        return this.translate.get('ITEM_ALREADY_IN_CART')
          .subscribe(str => this.showToast(str));
      }

      cart.items.push(filteredItem);

      await cart.save();

      this.item["existsInCart"] = true;
      this.item["qty"] = this.qty;


      this.isAddingToCart = false;

      window.dispatchEvent(new CustomEvent('cart:updated', {
        detail: cart
      }));

      const { value } = await this.showSweetSuccessView();

      if (value) {
        this.setRoot('/1/cart/checkout');
      }

    } catch (err) {
      console.log(err);
      this.isAddingToCart = false;
      if (err.code === 141) {
        this.translate.get('PLEASE_LOGIN').subscribe(str => this.showAlert(str));
      } else {
        this.translate.get('ERROR_NETWORK').subscribe(str => this.showAlert(str));
      }
    }

  }

  async checkIfItemInCart(item) {

    try {

      let cart = await this.cartService.getOne();

      if (cart) {

        if (item.id === this.item.id) {
          const existInCart = cart.items.filter(x => {
            if (!x.comboId) {
              return x.objectId == item.objectId;
            }
          });

          if (existInCart.length == 0) {
            this.item["existsInCart"] = false;
          } else if (existInCart) {
            this.item["qty"] = existInCart[0]['qty'];
            this.item["existsInCart"] = true;
          }
        }

      }

    } catch (err) {
      console.error(err);
    }

  }

  async decrementQuantity($event, item) {

    let qty = $event;
    let cart = await this.cartService.getOne();

    cart.items = cart.items.map((x) => {
      if (!x.comboId) {
        if (x.objectId === item.objectId) {
          if (x.qty > 0) {
            x.qty = qty;
            x.amount = x.qty * (x.salePrice || x.price);
          }
        }
      }
      return x;
    });

    this.preference.subtotal = cart.items.reduce((sum: number, item: any) => {
      return sum + item.amount;
    }, 0);

  }

  async incrementQuantity($event, item) {

    let qty = $event;

    let cart = await this.cartService.getOne();

    cart.items = cart.items.map((x) => {
      if (!x.comboId) {
        if (x.objectId === item.objectId) {
          x.qty = qty;
          x.amount = x.qty * (x.salePrice || x.price);
        }
      }
      return x;
    });

    this.preference.subtotal = cart.items.reduce((sum: number, item: any) => {
      return sum + item.amount;
    }, 0);

  }

}
