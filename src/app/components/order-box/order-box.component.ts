import { Component, OnInit, Input } from '@angular/core';
import { Order } from 'src/app/services/order';

@Component({
  selector: 'app-order-box',
  templateUrl: './order-box.component.html',
  styleUrls: ['./order-box.component.scss'],
})
export class OrderBoxComponent implements OnInit {

  @Input('order') order: Order;

  constructor() { }

  ngOnInit() {
    console.log(this.order);
    console.log(this.order.status.toLowerCase())
  }

}
