import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { UploadBoxComponent } from './upload-box/upload-box.component';
import { ShopItemComponent } from './shop-item/shop-item.component';
import { TranslateModule } from '@ngx-translate/core';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { AccordionComponent } from './accordion/accordion.component';
import { PipesModule } from '../pipes/pipes.module';
import { BarRatingModule } from "ngx-bar-rating";
import { ShopHeaderComponent } from './shop-header/shop-header.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { ComboItemBoxComponent } from './combo-item-box/combo-item-box.component';
import { ItemQtyComponent } from './item-qty-component/item-qty.component';
import { ItemWrapperComponent } from './item-wrapper/item-wrapper.component';
import { OrderBoxComponent } from './order-box/order-box.component';

@NgModule({
	declarations: [
		UploadBoxComponent,
		OrderBoxComponent,
		ShopItemComponent,
		AccordionComponent,
		ShopHeaderComponent,
		ComboItemBoxComponent,
		ItemQtyComponent,
		ItemWrapperComponent
	],
	imports: [
		CommonModule,
		IonicModule,
		BarRatingModule,
		TranslateModule,
		LazyLoadImageModule,
		PipesModule,
	],
	exports: [
		UploadBoxComponent,
		OrderBoxComponent,
		ShopItemComponent,
		AccordionComponent,
		ShopHeaderComponent,
		ComboItemBoxComponent,
		ItemQtyComponent,
		ItemWrapperComponent
	]
})
export class ComponentsModule { }
