import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ComboItem } from 'src/app/services/combo-item';

@Component({
  selector: 'combo-item-box',
  templateUrl: './combo-item-box.component.html',
  styleUrls: ['./combo-item-box.component.scss'],
})
export class ComboItemBoxComponent implements OnInit {

  @Input() item: ComboItem;
  @Input('customObservable') scrollObservable: Observable<any>;
  
  constructor() { }

  ngOnInit() {}

}
