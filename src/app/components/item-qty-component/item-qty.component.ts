import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EventManager } from '@angular/platform-browser';

@Component({
  selector: 'app-item-qty',
  templateUrl: './item-qty.component.html',
  styleUrls: ['./item-qty.component.scss'],
})
export class ItemQtyComponent implements OnInit {

  @Input() qty: number = 1;
  @Input() disabled: boolean = false;
  @Input() isShopItem: boolean = false;
  @Output('qtyIncrease') qtyIncrease: EventEmitter<any> = new EventEmitter<any>();
  @Output('qtyDecrease') qtyDecrease: EventEmitter<any> = new EventEmitter<any>(); 
  constructor() { }

  ngOnInit() {
    
  }

  incrementQuantity(){
    this.qty++;
    this.qtyIncrease.emit(this.qty);
  }

  decrementQuantity(){
    this.qty--;
    if(this.qty < 1){
      this.qty = 1;
    }
    this.qtyDecrease.emit(this.qty);
  }

}
