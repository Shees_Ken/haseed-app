import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { Observable } from 'rxjs';
import { ComboItem } from 'src/app/services/combo-item';
import { Item } from 'src/app/services/item';

@Component({
  selector: 'item-wrapper',
  templateUrl: './item-wrapper.component.html',
  styleUrls: ['./item-wrapper.component.scss'],
})
export class ItemWrapperComponent implements OnInit {

  @Input() items: [];
  @Input('customObservable') scrollObservable: Observable<any>;
  @Input('isBackgroundWhite') isBackgroundWhite = false;
  @Input('isOrderDetail') isOrderDetail = false;
  @Output('qtyIncrease') qtyIncrease: EventEmitter<any> = new EventEmitter<any>();
  @Output('qtyDecrease') qtyDecrease: EventEmitter<any> = new EventEmitter<any>();
  @Output('itemRemoved') itemRemoved: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() { 
    console.log(this.items);
  }

  removeItem(item) {
    this.itemRemoved.emit(item);
  }

  decrementQuantity(item) {
    if (item.qty > 0) {
      item.qty = item.qty - 1;
    }
    this.qtyDecrease.emit(item);
  }

  incrementQuantity(item) {
    item['qty']++;
    this.qtyIncrease.emit(item);
  }

}
