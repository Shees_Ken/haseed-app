import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { IonicModule } from '@ionic/angular';
import { EmptyView } from './empty-view';
import { SharedModule } from 'src/app/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    EmptyView,
  ],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule
  ],
  exports: [
    EmptyView
  ]
})
export class EmptyViewModule {}
