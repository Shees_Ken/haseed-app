import { Component, Injector, Input } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'empty-view',
  templateUrl: 'empty-view.html',
  styleUrls: ['empty-view.scss']
})
export class EmptyView extends BasePage {

  @Input() text: string = '';
  @Input() heading: string = '';
  @Input() icon: string = 'alert';
  @Input() image: string = '';
  @Input() imageWidth: string = '';
  @Input() showButton: boolean = false;
  @Input() isFav: boolean = false;

  constructor(
    injector: Injector
  ) {
    super(injector)
  }


  enableMenuSwipe() {
    return false;
  }

  goToHome() {
    this.navigateTo('1/home');
  }
}
